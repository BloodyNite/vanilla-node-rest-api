const http = require('http')
const { getDigimons, getDigimon, createDigimon, updateDigimon} = require('./controller/digimonController')
const server = http.createServer((req, res) => {
    //validamos URL y Metodo request
    if (req.url === '/api/digimons' && req.method === 'GET') {
        getDigimons(req, res)
    } else if(req.url.match(/\/api\/digimons\/([0-9]+)/) && req.method === 'GET'){
        const id = req.url.split('/')[3] //Obtenemos el numero en la posicion 3 del array en la url
        getDigimon(req, res, id)
    } else if(req.url === '/api/digimons' && req.method === 'POST'){
        createDigimon(req, res)
    } else if(req.url.match(/\/api\/digimons\/([0-9]+)/) && req.method === 'PUT'){
        const id = req.url.split('/')[3]
        updateDigimon(req, res, id)
    } else {
        res.writeHead(404, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify({ message: 'Route Not Found' }))
    }
})
const PORT = process.env.PORT || 5000
server.listen(PORT, () => console.log(`Server running on port ${PORT}`))