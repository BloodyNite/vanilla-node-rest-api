const digimons = require('../data/digimons')
const { v4: uuidv4 } = require('uuid')

const { writeDataToFile } = require('../utils')

function findAll() {
    return new Promise((resolve, reject) => {
        resolve(digimons)
    })
}

function findById(id) {
    return new Promise((resolve, reject) => {
        const digimon = digimons.find((d) => String(d.ID) === id)
        resolve(digimon)
    })
}

function create(digimon) {
    return new Promise((resolve, reject) => {
        const newDigimon = { ID: uuidv4(), ...digimon }
        digimons.push(newDigimon)
        writeDataToFile('./data/digimons.json', digimons)
        resolve(newDigimon)
    })
}
function update(ID, digimon) {
    return new Promise((resolve, reject) => {
        //Busco el id recibido en Json Data y lo guardo en la constante
        const index = digimons.findIndex((d) => String(d.ID) === ID)
        //Guardo la data recibida en el ID recibido
        digimons[index] = { ID, ...digimon }
        writeDataToFile('./data/digimons.json', digimons)
        resolve(digimons[index])
    })
}

module.exports = {
    findAll,
    findById,
    create,
    update
}