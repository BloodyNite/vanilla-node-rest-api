const Digimon = require('../models/digimonModel')
const { getPostData } = require('../utils')

//@desc Obtiene todos los digimons
//@route GET /api/digimons
async function getDigimons(req, res) {
    try {
        const digimons = await Digimon.findAll()
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify(digimons))
    } catch (error) {
        console.log(error);
    }
}

//@desc Obtiene digimon por ID
//@route GET /api/digimons/:id
async function getDigimon(req, res, id) {
    try {
        const digimon = await Digimon.findById(id)

        if (!digimon) {
            res.writeHead(404, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify({ message: 'Digimon no encontrado' }))
        } else {
            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify(digimon))
        }

    } catch (error) {
        console.log(error);
    }
}

//@desc Crea un digimon
//@route POST /api/digimons
async function createDigimon(req, res) {
    try {
        const body = await getPostData(req)
        const { name, img, level } = JSON.parse(body)
        const digimon = {
            name, img, level
        }
        const newDigimon = await Digimon.create(digimon)
        res.writeHead(201, { 'Content-Type': 'application/json' })
        return res.end(JSON.stringify(newDigimon))
    } catch (error) {
        console.log(error);
    }
}

//@desc Actualiza un digimon
//@route PUT /api/digimons/:id
async function updateDigimon(req, res, id) {
    try {
        const digimon = await Digimon.findById(id)
        if (!digimon) {
            res.writeHead(404, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify({ message: 'Digimon no encontrado' }))
        } else {

            const body = await getPostData(req)

            const { name, img, level } = JSON.parse(body)
            
            const digimonData = {
                name: name || digimon.name,
                img: img || digimon.img,
                level: level || digimon.level
            }
            const updDigimon = await Digimon.update(id, digimonData)
            res.writeHead(200, { 'Content-Type': 'application/json' })
            return res.end(JSON.stringify(updDigimon))
        }

    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    getDigimons,
    getDigimon,
    createDigimon,
    updateDigimon
}
